const assert = require('assert')
const { sum, diff, prod, qoutient } = require('../src/sum')

describe('Summation Function', () => {
    it('should add 5 and 3 to 8', () => {
        assert.equal(sum(5,3), 8)
    })
    it('should not add 5 and 2 to 7', () => {
        assert.equal(sum(5,3) === 7, false)
    })
    it('should not add null and nuill', () => {
        assert.equal(sum(null,null), null)
    })
})

describe('Subtraction Function', () => {
    it('should subtract 5 and 3 to 2', () => {
        assert.equal(diff(5,3), 2)
    })
    it('should not subtract 5 and 2 to 4', () => {
        assert.equal(diff(5,2) === 4, false)
    })
    it('should not subract null and nuill', () => {
        assert.equal(diff(null,null), null)
    })
})

describe('Multiplication Function', () => {
    it('should multiply 5 and 3 to 15', () => {
        assert.equal(prod(5,3), 15)
    })
    it('should not multiply 5 and 2 to 11', () => {
        assert.equal(prod(5,3) === 11, false)
    })
    it('should not add null and nuill', () => {
        assert.equal(prod(null,null), null)
    })
})

describe('Divition Function', () => {
    it('should divide 4 and 2 to 2', () => {
        assert.equal(qoutient(4,2), 2)
    })
    it('should not divide 10 and 2 to 6', () => {
        assert.equal(qoutient(5,3) === 6, false)
    })
    it('should not add null and nuill', () => {
        assert.equal(qoutient(null,null), null)
    })
})