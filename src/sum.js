const sum = (x,y) => {
    if ( x === null && y === null) {
        return null
    }
    return x + y
}

const diff = (x,y) => {
    if ( x === null && y === null) {
        return null
    }
    return x - y
}

const prod = (x,y) => {
    if ( x === null && y === null) {
        return null
    }
    return x * y
}

const qoutient = (x,y) => {
    if ( x === null && y === null) {
        return null
    }
    return x / y
}
module.exports = {
    sum,
    diff,
    prod,
    qoutient,
}